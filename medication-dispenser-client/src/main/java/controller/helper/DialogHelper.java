package controller.helper;

import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;

import java.util.Optional;
import java.util.function.Supplier;

public class DialogHelper {
    public static void showTakenDialog(String message) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Take medication.");
        alert.setContentText(message);

        alert.showAndWait();
    }

    public static <R> Optional<R> showCustomDialog(String title, Node content, Supplier<R> dialogDataSupplier) {
        Dialog<R> dialog = new Dialog<>();
        dialog.setTitle(title);
        dialog.getDialogPane().setContent(content);
        dialog.getDialogPane().getButtonTypes().addAll(ButtonType.OK, ButtonType.CANCEL);
        dialog.setResizable(true);

        dialog.setResultConverter(dialogButton -> {
            if (dialogButton.getButtonData() == ButtonBar.ButtonData.OK_DONE) {
                return dialogDataSupplier.get();
            }
            return null;
        });
        return dialog.showAndWait();
    }
}
