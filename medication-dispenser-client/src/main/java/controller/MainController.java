package controller;

import controller.helper.DialogHelper;
import grpc.Drug;
import grpc.MedicalPlanServiceGrpc;
import grpc.MedicationPlan;
import grpc.Text;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.util.Duration;

import java.net.URL;
import java.time.LocalTime;
import java.util.ResourceBundle;

public class MainController implements Initializable {

    private MedicationPlan medicalPlan;

    private MedicalPlanServiceGrpc.MedicalPlanServiceBlockingStub stub;

    @FXML
    private TableView<Drug> medicationView = new TableView<>();

    @FXML
    private TableColumn<Drug, String> nameCol;

    @FXML
    private TableColumn<Drug, String> effectsCol;

    @FXML
    private TableColumn<Drug, String> dosageCol;

    @FXML
    private TableColumn<Drug, String> takenCol;

    @FXML
    private TableColumn<Drug, String> startCol;

    @FXML
    private TableColumn<Drug, String> endCol;

    @FXML
    private Button takeButton;

    @FXML
    private Label time;

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        nameCol.setCellValueFactory(new PropertyValueFactory<>("name"));
        effectsCol.setCellValueFactory(new PropertyValueFactory<>("sideEffects"));
        dosageCol.setCellValueFactory(new PropertyValueFactory<>("dosage"));
        takenCol.setCellValueFactory(new PropertyValueFactory<>("taken"));
        startCol.setCellValueFactory(new PropertyValueFactory<>("intakeStart"));
        endCol.setCellValueFactory(new PropertyValueFactory<>("intakeEnd"));

        Timeline clock = new Timeline(new KeyFrame(Duration.ZERO, e -> {
            LocalTime currentTime = LocalTime.now();
            time.setText(currentTime.getHour() + ":" + currentTime.getMinute());
        }),
                new KeyFrame(Duration.seconds(1))
        );
        clock.setCycleCount(Animation.INDEFINITE);
        clock.play();

        ManagedChannel channel = ManagedChannelBuilder.forAddress("localhost", 6565)
                .usePlaintext()
                .build();

        stub = MedicalPlanServiceGrpc.newBlockingStub(channel);

        setTableContents();

        takeButton.setOnAction(e -> {
            Drug medication = medicationView.getSelectionModel().getSelectedItem();
            Text response = stub.takeMedication(medication);
            DialogHelper.showTakenDialog(response.getText());
            if(response.getText().equals("You have successfully taken your medication!")) {
                medicationView.setItems(null);
                setTableContents();
            }
        });

        //channel.shutdown();

    }

    private void setTableContents() {
        medicalPlan = stub.getMedicalPlan(Text.newBuilder().setText("1").build());

        ObservableList<Drug> medications = FXCollections.observableArrayList();

        for(Drug medication : medicalPlan.getMedicationsList()){
            medications.add(medication);
        }

        medicationView.setItems(medications);
    }
}