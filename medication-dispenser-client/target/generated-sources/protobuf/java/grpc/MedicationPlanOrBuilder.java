// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: MedicationPlan.proto

package grpc;

public interface MedicationPlanOrBuilder extends
    // @@protoc_insertion_point(interface_extends:grpc.MedicationPlan)
    com.google.protobuf.MessageOrBuilder {

  /**
   * <code>string treatmentPeriod = 1;</code>
   */
  java.lang.String getTreatmentPeriod();
  /**
   * <code>string treatmentPeriod = 1;</code>
   */
  com.google.protobuf.ByteString
      getTreatmentPeriodBytes();

  /**
   * <code>repeated .grpc.Drug medications = 2;</code>
   */
  java.util.List<grpc.Drug> 
      getMedicationsList();
  /**
   * <code>repeated .grpc.Drug medications = 2;</code>
   */
  grpc.Drug getMedications(int index);
  /**
   * <code>repeated .grpc.Drug medications = 2;</code>
   */
  int getMedicationsCount();
  /**
   * <code>repeated .grpc.Drug medications = 2;</code>
   */
  java.util.List<? extends grpc.DrugOrBuilder> 
      getMedicationsOrBuilderList();
  /**
   * <code>repeated .grpc.Drug medications = 2;</code>
   */
  grpc.DrugOrBuilder getMedicationsOrBuilder(
      int index);
}
