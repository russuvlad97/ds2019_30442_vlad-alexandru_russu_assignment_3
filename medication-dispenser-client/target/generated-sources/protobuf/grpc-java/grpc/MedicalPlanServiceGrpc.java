package grpc;

import static io.grpc.stub.ClientCalls.asyncUnaryCall;
import static io.grpc.stub.ClientCalls.asyncServerStreamingCall;
import static io.grpc.stub.ClientCalls.asyncClientStreamingCall;
import static io.grpc.stub.ClientCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ClientCalls.blockingUnaryCall;
import static io.grpc.stub.ClientCalls.blockingServerStreamingCall;
import static io.grpc.stub.ClientCalls.futureUnaryCall;
import static io.grpc.MethodDescriptor.generateFullMethodName;
import static io.grpc.stub.ServerCalls.asyncUnaryCall;
import static io.grpc.stub.ServerCalls.asyncServerStreamingCall;
import static io.grpc.stub.ServerCalls.asyncClientStreamingCall;
import static io.grpc.stub.ServerCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedStreamingCall;

/**
 */
@javax.annotation.Generated(
    value = "by gRPC proto compiler (version 1.4.0)",
    comments = "Source: MedicationPlan.proto")
public final class MedicalPlanServiceGrpc {

  private MedicalPlanServiceGrpc() {}

  public static final String SERVICE_NAME = "grpc.MedicalPlanService";

  // Static method descriptors that strictly reflect the proto.
  @io.grpc.ExperimentalApi("https://github.com/grpc/grpc-java/issues/1901")
  public static final io.grpc.MethodDescriptor<grpc.Text,
      grpc.MedicationPlan> METHOD_GET_MEDICAL_PLAN =
      io.grpc.MethodDescriptor.<grpc.Text, grpc.MedicationPlan>newBuilder()
          .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
          .setFullMethodName(generateFullMethodName(
              "grpc.MedicalPlanService", "getMedicalPlan"))
          .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
              grpc.Text.getDefaultInstance()))
          .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
              grpc.MedicationPlan.getDefaultInstance()))
          .build();
  @io.grpc.ExperimentalApi("https://github.com/grpc/grpc-java/issues/1901")
  public static final io.grpc.MethodDescriptor<grpc.Drug,
      grpc.Text> METHOD_TAKE_MEDICATION =
      io.grpc.MethodDescriptor.<grpc.Drug, grpc.Text>newBuilder()
          .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
          .setFullMethodName(generateFullMethodName(
              "grpc.MedicalPlanService", "takeMedication"))
          .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
              grpc.Drug.getDefaultInstance()))
          .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
              grpc.Text.getDefaultInstance()))
          .build();

  /**
   * Creates a new async stub that supports all call types for the service
   */
  public static MedicalPlanServiceStub newStub(io.grpc.Channel channel) {
    return new MedicalPlanServiceStub(channel);
  }

  /**
   * Creates a new blocking-style stub that supports unary and streaming output calls on the service
   */
  public static MedicalPlanServiceBlockingStub newBlockingStub(
      io.grpc.Channel channel) {
    return new MedicalPlanServiceBlockingStub(channel);
  }

  /**
   * Creates a new ListenableFuture-style stub that supports unary calls on the service
   */
  public static MedicalPlanServiceFutureStub newFutureStub(
      io.grpc.Channel channel) {
    return new MedicalPlanServiceFutureStub(channel);
  }

  /**
   */
  public static abstract class MedicalPlanServiceImplBase implements io.grpc.BindableService {

    /**
     */
    public void getMedicalPlan(grpc.Text request,
        io.grpc.stub.StreamObserver<grpc.MedicationPlan> responseObserver) {
      asyncUnimplementedUnaryCall(METHOD_GET_MEDICAL_PLAN, responseObserver);
    }

    /**
     */
    public void takeMedication(grpc.Drug request,
        io.grpc.stub.StreamObserver<grpc.Text> responseObserver) {
      asyncUnimplementedUnaryCall(METHOD_TAKE_MEDICATION, responseObserver);
    }

    @java.lang.Override public final io.grpc.ServerServiceDefinition bindService() {
      return io.grpc.ServerServiceDefinition.builder(getServiceDescriptor())
          .addMethod(
            METHOD_GET_MEDICAL_PLAN,
            asyncUnaryCall(
              new MethodHandlers<
                grpc.Text,
                grpc.MedicationPlan>(
                  this, METHODID_GET_MEDICAL_PLAN)))
          .addMethod(
            METHOD_TAKE_MEDICATION,
            asyncUnaryCall(
              new MethodHandlers<
                grpc.Drug,
                grpc.Text>(
                  this, METHODID_TAKE_MEDICATION)))
          .build();
    }
  }

  /**
   */
  public static final class MedicalPlanServiceStub extends io.grpc.stub.AbstractStub<MedicalPlanServiceStub> {
    private MedicalPlanServiceStub(io.grpc.Channel channel) {
      super(channel);
    }

    private MedicalPlanServiceStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected MedicalPlanServiceStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new MedicalPlanServiceStub(channel, callOptions);
    }

    /**
     */
    public void getMedicalPlan(grpc.Text request,
        io.grpc.stub.StreamObserver<grpc.MedicationPlan> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(METHOD_GET_MEDICAL_PLAN, getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void takeMedication(grpc.Drug request,
        io.grpc.stub.StreamObserver<grpc.Text> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(METHOD_TAKE_MEDICATION, getCallOptions()), request, responseObserver);
    }
  }

  /**
   */
  public static final class MedicalPlanServiceBlockingStub extends io.grpc.stub.AbstractStub<MedicalPlanServiceBlockingStub> {
    private MedicalPlanServiceBlockingStub(io.grpc.Channel channel) {
      super(channel);
    }

    private MedicalPlanServiceBlockingStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected MedicalPlanServiceBlockingStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new MedicalPlanServiceBlockingStub(channel, callOptions);
    }

    /**
     */
    public grpc.MedicationPlan getMedicalPlan(grpc.Text request) {
      return blockingUnaryCall(
          getChannel(), METHOD_GET_MEDICAL_PLAN, getCallOptions(), request);
    }

    /**
     */
    public grpc.Text takeMedication(grpc.Drug request) {
      return blockingUnaryCall(
          getChannel(), METHOD_TAKE_MEDICATION, getCallOptions(), request);
    }
  }

  /**
   */
  public static final class MedicalPlanServiceFutureStub extends io.grpc.stub.AbstractStub<MedicalPlanServiceFutureStub> {
    private MedicalPlanServiceFutureStub(io.grpc.Channel channel) {
      super(channel);
    }

    private MedicalPlanServiceFutureStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected MedicalPlanServiceFutureStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new MedicalPlanServiceFutureStub(channel, callOptions);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<grpc.MedicationPlan> getMedicalPlan(
        grpc.Text request) {
      return futureUnaryCall(
          getChannel().newCall(METHOD_GET_MEDICAL_PLAN, getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<grpc.Text> takeMedication(
        grpc.Drug request) {
      return futureUnaryCall(
          getChannel().newCall(METHOD_TAKE_MEDICATION, getCallOptions()), request);
    }
  }

  private static final int METHODID_GET_MEDICAL_PLAN = 0;
  private static final int METHODID_TAKE_MEDICATION = 1;

  private static final class MethodHandlers<Req, Resp> implements
      io.grpc.stub.ServerCalls.UnaryMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ServerStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ClientStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.BidiStreamingMethod<Req, Resp> {
    private final MedicalPlanServiceImplBase serviceImpl;
    private final int methodId;

    MethodHandlers(MedicalPlanServiceImplBase serviceImpl, int methodId) {
      this.serviceImpl = serviceImpl;
      this.methodId = methodId;
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public void invoke(Req request, io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        case METHODID_GET_MEDICAL_PLAN:
          serviceImpl.getMedicalPlan((grpc.Text) request,
              (io.grpc.stub.StreamObserver<grpc.MedicationPlan>) responseObserver);
          break;
        case METHODID_TAKE_MEDICATION:
          serviceImpl.takeMedication((grpc.Drug) request,
              (io.grpc.stub.StreamObserver<grpc.Text>) responseObserver);
          break;
        default:
          throw new AssertionError();
      }
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public io.grpc.stub.StreamObserver<Req> invoke(
        io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        default:
          throw new AssertionError();
      }
    }
  }

  private static final class MedicalPlanServiceDescriptorSupplier implements io.grpc.protobuf.ProtoFileDescriptorSupplier {
    @java.lang.Override
    public com.google.protobuf.Descriptors.FileDescriptor getFileDescriptor() {
      return grpc.MedicationPlanOuterClass.getDescriptor();
    }
  }

  private static volatile io.grpc.ServiceDescriptor serviceDescriptor;

  public static io.grpc.ServiceDescriptor getServiceDescriptor() {
    io.grpc.ServiceDescriptor result = serviceDescriptor;
    if (result == null) {
      synchronized (MedicalPlanServiceGrpc.class) {
        result = serviceDescriptor;
        if (result == null) {
          serviceDescriptor = result = io.grpc.ServiceDescriptor.newBuilder(SERVICE_NAME)
              .setSchemaDescriptor(new MedicalPlanServiceDescriptorSupplier())
              .addMethod(METHOD_GET_MEDICAL_PLAN)
              .addMethod(METHOD_TAKE_MEDICATION)
              .build();
        }
      }
    }
    return result;
  }
}
