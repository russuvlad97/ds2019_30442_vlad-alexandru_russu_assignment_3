package ro.utcn.onlinemedicationplatform.dto.builders;

import ro.utcn.onlinemedicationplatform.dto.view.CaregiverViewDTO;
import ro.utcn.onlinemedicationplatform.entities.Caregiver;

public class CaregiverViewBuilder {

    public static CaregiverViewDTO generateDTOFromEntity(Caregiver caregiver){
        return new CaregiverViewDTO(
                caregiver.getId(),
                caregiver.getName(),
                caregiver.getPatients()
        );
    }

    public static Caregiver generateEntityFromDTO(CaregiverViewDTO caregiverViewDTO){
        return new Caregiver(
                caregiverViewDTO.getName(),
                caregiverViewDTO.getPatients()
        );
    }
}
