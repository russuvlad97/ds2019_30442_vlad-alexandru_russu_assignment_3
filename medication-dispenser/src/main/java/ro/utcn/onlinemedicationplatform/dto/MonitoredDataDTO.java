package ro.utcn.onlinemedicationplatform.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class MonitoredDataDTO {

    private Long id;
    private LocalDateTime startTime;
    private LocalDateTime endTime;
    private String activity;
    private Long pId;

}
