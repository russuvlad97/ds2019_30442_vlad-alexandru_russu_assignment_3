package ro.utcn.onlinemedicationplatform.dto.builders;

import lombok.NoArgsConstructor;
import ro.utcn.onlinemedicationplatform.dto.PatientDTO;
import ro.utcn.onlinemedicationplatform.entities.Patient;

@NoArgsConstructor
public class PatientBuilder {

    public static PatientDTO generateDTOFromEntity(Patient patient){
        return new PatientDTO(
                patient.getId(),
                patient.getName(),
                patient.getUsername(),
                patient.getPassword(),
                patient.getBirth_date(),
                patient.getGender(),
                patient.getAddress(),
                patient.getMedicalRecord(),
                patient.getMedicalPlan(),
                patient.getCaregiver()

        );
    }

    public static Patient generateEntityFromDTO(PatientDTO patientDTO){
        return new Patient(
                patientDTO.getName(),
                patientDTO.getUsername(),
                patientDTO.getPassword(),
                patientDTO.getBirthDate(),
                patientDTO.getGender(),
                patientDTO.getAddress(),
                patientDTO.getMedicalRecord(),
                patientDTO.getMedicalPlan(),
                patientDTO.getCaregiver()
        );

    }
}