package ro.utcn.onlinemedicationplatform.services;

import grpc.*;
import io.grpc.stub.StreamObserver;
import org.lognet.springboot.grpc.GRpcService;
import org.springframework.transaction.annotation.Transactional;
import ro.utcn.onlinemedicationplatform.entities.Medication;
import ro.utcn.onlinemedicationplatform.entities.MedicalPlan;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@GRpcService
@Transactional
public class MedicalPlanServiceImpl extends MedicalPlanServiceGrpc.MedicalPlanServiceImplBase {

    private final MedicalPlanService medicalPlanService;
    private final MedicationService medicationService;

    public MedicalPlanServiceImpl(MedicalPlanService medicalPlanService, MedicationService medicationService) {
        this.medicalPlanService = medicalPlanService;
        this.medicationService = medicationService;
    }

    @Override
    public void getMedicalPlan(Text request, StreamObserver<MedicationPlan> responseObserver){

        Long id = Long.parseLong(request.getText());
        MedicalPlan medicalPlan = medicalPlanService.findMedicalPlanById(id);

        List<Drug.Builder> medicationList = new ArrayList<>();

        for(Medication medication : medicalPlan.getMedications()){
            medicationList.add(Drug.newBuilder()
                                .setId(medication.getId())
                                .setName(medication.getName())
                                .setDosage(medication.getDosage())
                                .setSideEffects(medication.getSideEffects())
                                .setTaken(medication.isTaken())
                                .setIntakeStart(medication.getIntakeStart())
                                .setIntakeEnd(medication.getIntakeEnd()));
        }

        Iterable<Drug> medications = medicationList.stream().map(x->x.build()).collect(Collectors.toList());

        MedicationPlan medicationPlan = MedicationPlan.newBuilder()
                .setTreatmentPeriod(medicalPlan.getTreatmentPeriod())
                .addAllMedications(medications)
                .build();

        responseObserver.onNext(medicationPlan);
        responseObserver.onCompleted();
    }

    @Override
    public void takeMedication(Drug request, StreamObserver<Text> responseObserver){

        Text text;
        LocalTime time = LocalTime.now();
        LocalTime start = LocalTime.of(request.getIntakeStart(), 0);
        LocalTime end = LocalTime.of(request.getIntakeEnd(), 0);

        if( time.isAfter(start) && time.isBefore(end) ) {
            text = Text.newBuilder().setText("You have successfully taken your medication!").build();
            medicationService.setTaken(request.getId());
        }
        else {
            text = Text.newBuilder().setText("You cannot take your medication outside the intake interval!").build();
        }
        responseObserver.onNext(text);
        responseObserver.onCompleted();
    }

}
