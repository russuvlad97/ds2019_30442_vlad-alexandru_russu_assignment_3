package ro.utcn.onlinemedicationplatform.entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Medication {

    @Id
    @GeneratedValue(
            strategy = GenerationType.AUTO,
            generator = "native"
    )
    @GenericGenerator(
            name = "native",
            strategy = "native"
    )
    private Long id;

    @Column
    private String name;

    @Column
    private String sideEffects;

    @Column
    private String dosage;

    @Column
    private boolean taken;

    @Column
    private int intakeStart;

    @Column
    private int intakeEnd;

    @ManyToMany(mappedBy = "medications")
    private List<MedicalPlan> medicalPlans = new ArrayList<>();

    public Medication(String name, String sideEffects, String dosage) {
        this.name = name;
        this.sideEffects = sideEffects;
        this.dosage = dosage;
    }

    public Medication(Long id, String name, String sideEffects, String dosage, boolean taken, int intakeStart, int intakeEnd) {
        this.id = id;
        this.name = name;
        this.sideEffects = sideEffects;
        this.dosage = dosage;
        this.taken = taken;
        this.intakeStart = intakeStart;
        this.intakeEnd = intakeEnd;
    }
}
